import React, { useState, useEffect, useContext } from "react";
import {firebaseAuth} from '../../provider/AuthProvider';
import { LazyLoadComponent } from 'react-lazy-load-image-component';

import Cards from '../CardsHome';
import { fire, db } from '../../fire'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Header from "../header";
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Search from "../search";


const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  header:{
    backgroundColor: '#231722c7',
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));
const Home = (props) => {
  const menuId = 'primary-search-account-menu';

    const [restaurants, setRestaurants] = useState([]);
    const {handleSignout} = useContext(firebaseAuth)
    const classes = useStyles();        
    const handleConnexion = (event) => {
      props.history.push("/signin");
    };
    useEffect(() => {
        //Database Cloud Firestore
        console.log(window.localStorage.token);
      const restos = [];
      db.collection("restaurants")
      .get()
      .then(querySnapshot => {
          const data = querySnapshot.docs.forEach(doc => {
            const resto = new Object();
            resto.id =  doc.id;
            resto.data = doc.data();
            console.log(resto.data);
            restos.push(resto);
          });
          setRestaurants(restos); 
      });
  

    }, []);
    
    return (
    <>

 <AppBar position="static" >
        <Toolbar  className={classes.header}>
    
          <Typography className={classes.title} variant="h4" noWrap>
            Open Eat
          </Typography>
          
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
          <Button variant="contained" onClick={handleConnexion}>Connexion</Button>
            
          </div>
          
        </Toolbar>
      </AppBar>

      <Paper className={classes.paper} className='header' >
                {/* Increase the priority of the hero background image */}
                <Container maxWidth="md">
                    <Grid height="100px" align="center" >
                        <Typography variant="h3" color='tan' >
                            Commandez facilement vos repas préférés sur OPEN EAT
                        </Typography>

                    </Grid>
                </Container>

            </Paper>
    
            <br></br>

        <Container  maxWidth="md">
          <Grid container spacing={4}>
                {
                restaurants && restaurants.map((resto) => 
                    <LazyLoadComponent>
                      <Cards id={resto.id} resto={resto.data}/>
                    </LazyLoadComponent>
                )
                }
            </Grid>
        </Container>
    </>
    )
    ;
}

export default Home;
