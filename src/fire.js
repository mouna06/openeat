import firebase from 'firebase'
var config = {
    apiKey: "AIzaSyCLIKnKB3Y_QjmYot8boAx8Q6RkDXEyEkM",
    authDomain: "fir-ae6f6.firebaseapp.com",
    databaseURL: "https://fir-ae6f6.firebaseio.com",
    projectId: "fir-ae6f6",
    storageBucket: "fir-ae6f6.appspot.com",
    messagingSenderId: "310757537811",
    appId: "1:310757537811:web:b95c42bcb8b992b00c27e6",
    measurementId: "G-K8NMR0J6B2"
  };
export const fire = firebase.initializeApp(config);
const messaging = firebase.messaging();
messaging.usePublicVapidKey("BABHjE_-_5nRBXTpOqytK_WHyFn5z9YoK85hOctvpimixnS9YhQqOTCPoFJ4YGyVElIQnG_jDUIoPQbBHn5P7oQ");
console.log(messaging);
export const db = fire.firestore();
firebase.analytics();
firebase.auth();
export default {
    config
  }