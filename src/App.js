
import './App.css';
import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";
import Signup from './components/sign/signup';
import Signin from './components/sign/signin';
import Home from './components/sign/home';
import Restaurant from "./components/restaurant";
import Menus from "./components/menus";
import Commande from "./components/commande";
import Favorites from "./components/Favorites";
import AuthProvider from './provider/AuthProvider';
export default function App() {

  return (
    <Router>
      <AuthProvider>
        <section className="app">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/" component={Restaurant} />
            <Route exact path="/Favorites" component={Favorites} />
            <Route exact path='/home' component={Restaurant} />
            <Route exact path="/home/commandes" component={Commande} />
            <Route exact path="/home/:id" component={Menus} />
            <Route exact path='/signin' component={Signin} />
            <Route exact path='/signup' component={Signup} />
          </Switch>
        </section>
      </AuthProvider>
    </Router>
  )
}
