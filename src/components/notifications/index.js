import addNotification from 'react-push-notification';
import React from 'react';

const Page = () => {
 
    const buttonClick = () => {
        addNotification({
            title: 'Commande Prise en charge par nos collaborateurs',
            message: 'pour votre confiance',
            duration: 5000, //optional, default: 5000,

            theme: 'light',
            native: true // when using native, your OS will handle theming.
        });
    };
 
    return (
        <>
      <div className="page">
          <button onClick={buttonClick} className="button">
           Hello world.
          </button>
      </div>
      </>
    );
  }

 
export default Page;