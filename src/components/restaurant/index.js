import React, { useState, useEffect, useContext } from "react";
import { Offline, Online } from "react-detect-offline";
import Cards from '../Cards';
import { fire, db } from '../../fire'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Header from "../header";
import Search from "../search";
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
    search: {
        position: 'relative'
    },

}));
const Restaurant = ({ }) => {
    const classes = useStyles();

    const [restaurants, setRestaurants] = useState([]);
    const [boolean, setBoolean] = useState(false);

    useEffect(() => {
        setBoolean(true);
    });
    useEffect(() => {
        const restos = [];
        db.collection("restaurants")
            .get()
            .then(querySnapshot => {
                querySnapshot.docs.forEach(doc => {
                    const resto = new Object();
                    resto.id = doc.id;
                    resto.data = doc.data();
                    restos.push(resto);
                });
                setRestaurants(restos);
            });

    }, [boolean]);
    return (
        <>
            <Header></Header>
            <Paper className={classes.paper} className='header' >
                {/* Increase the priority of the hero background image */}
                <Container maxWidth="md">

                    <Grid height="100px" align="center" >

                        <Typography variant="h3" color='tan' >

                            Commandez facilement vos repas préférés sur OPEN EAT
                        </Typography>

                        <Search className={classes.search} restos={restaurants}></Search>

                    </Grid>
                </Container>

            </Paper>
              <Offline>
 <Typography variant="h2" align="center" color="textPrimary" gutterBottom>
 Il semblerait que vous n'êtes pas connecté à Internet. Réessayez plus tard pour voir les produits du restaurant
        </Typography>
            </Offline> 

            <Typography variant="h2" align="center" color="textPrimary" gutterBottom>
                Nos Restaurants
        </Typography>
            <br></br>

            <Container maxWidth="md">
                <Grid container spacing={4}>
                    {
                        restaurants && restaurants.map(val =>
                            <Cards id={val.id} resto={val.data} />
                        )
                    }
                </Grid>
            </Container>

        </>
    )
        ;
}

export default Restaurant;
