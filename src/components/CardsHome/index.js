import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardHeader from '@material-ui/core/CardHeader';
import { makeStyles } from '@material-ui/core/styles';
import React from "react";
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';



const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    width: '31%',
    margin: '1%',
  },
  media: {
    height: 140,
  },
});

const Cards = ({ id, resto }) => {
  const classes = useStyles();


  return (
    <Card className={classes.root}>
      <CardActionArea>

        <LazyLoadImage
          effect="blur"
          src={"https://source.unsplash.com/user/ounachaibi"} height='300px'/>

        <CardHeader
          title={resto.nom}
          subheader={resto.adresse}
        />
      </CardActionArea>
    </Card>

  );
}

export default Cards;
