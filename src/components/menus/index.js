import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogActions from '@material-ui/core/DialogActions';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import React, { useEffect, useState } from "react";
import addNotification from 'react-push-notification';
import { db, fire } from '../../fire';
import CardsMenu from '../CardsMenu';
import Comments from '../CommentsNew';
import Header from "../header";
import { Offline, Online } from "react-detect-offline";


const useStyles = makeStyles((theme) => ({
  mainFeaturedPost: {
    position: 'relative',
    backgroundColor: theme.palette.grey[800],
    color: theme.palette.common.white,
    marginBottom: theme.spacing(4),
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: 'rgba(0,0,0,.3)',
  },
  mainFeaturedPostContent: {
    position: 'relative',
    padding: theme.spacing(3),
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(6),
      paddingRight: 0,
    },
  },
}));
const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  }

});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;

  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});


const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);
const Menus = ({ }) => {
  const classes = useStyles();
  const idResto = window.location.pathname.split('/')[2];
  const [menus, setMenus] = useState([]);
  const [resto, setResto] = useState([]);
  const [commande, setCommande] = useState([]);
  const [value, setValue] = useState(false);

  function handlerCommande() {
    commande.map((val) => {
      fire.database().ref('Commandes').push({ content: val, idRestaurant: idResto, idUtilisateur: window.localStorage.uid });

    })

    addNotification({
      title: 'Commande Prise en charge par nos collaborateurs',
      message: 'Merci pour votre confiance',
      duration: 10000, //optional, default: 5000,

      theme: 'light',
      native: true // when using native, your OS will handle theming.
    });
    console.log(commande);
  }

  function handlerStateMyState(newState, count) {
    console.log(newState);
    commande.push(newState);
  };

  function renderPopup() {
    // enregistrer la commande dans firebase ici 
    console.log(window.localStorage.uid);

    return (
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"VOTRE COMMANDE A BIEN ÉTÉ PRISE EN COMPTE"}</DialogTitle>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Fermer
       </Button>
        </DialogActions>
      </Dialog>
    )
  };

  useEffect(() => {

    var restos = [];

    var docRef = db.collection("restaurants").doc(idResto);
    docRef.get().then(function (doc) {
      if (doc.exists) {
        restos = doc.data();
        setResto(restos);
      } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
      }
    }).catch(function (error) {
      console.log("Error getting document:", error);
    });

  }, [value]);
  useEffect(() => {
    setValue(true);
  });

  useEffect(() => {
    //Database Cloud Firestore

    db.collection("menus").where("idRestaurant", "==", idResto)
      .get()
      .then(querySnapshot => {
        const data = querySnapshot.docs.map(doc => doc.data());
        setMenus(data);
      });
  }, [value]);
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {

    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>

      <Header commande={commande}></Header>
     
      <Paper className={classes.mainFeaturedPost} style={{ effect: 'blur', backgroundImage: `url(https://source.unsplash.com/user/ounachaibi)` }}>
        {/* Increase the priority of the hero background image */}
        {<img style={{ display: 'none' }} />}
        <div className={classes.overlay} />
        <Grid container>
          <Grid item md={6}>
            <div className={classes.mainFeaturedPostContent}>
              <Typography component="h1" variant="h3" color="inherit" gutterBottom>
                {resto.nom}
              </Typography>
              <Typography variant="h5" color="inherit" paragraph>
                {resto.adresse}
              </Typography>

            </div>
          </Grid>
        </Grid>
      </Paper>
    <Offline >

    Il semblerait que vous n'êtes pas connecté à Internet, verifiez votre connexion
    <hr />
      <br></br>
</Offline>
      <Container fixed spacing={2} >
        <Grid item xs={12} justify="center" display='inline-block' container spacing={2} >
          {
            menus && menus.map((menu) =>
              <CardsMenu menu={menu} handlerStateMyState={handlerStateMyState} />
            )
          }

        </Grid>


        <hr />
        {renderPopup()}
        <Grid justify="center" container >

          <Button variant="contained" color="secondary" onClick={handlerCommande} >
            Commander
          </Button>
        </Grid>
        <Grid align="center" >
          <Comments idResto={idResto}></Comments>


        </Grid>

      </Container>

    </>
  )
    ;
}

export default Menus;
