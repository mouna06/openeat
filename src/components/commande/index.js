import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import React, { useEffect, useState } from "react";
import { Offline } from "react-detect-offline";
import { fire } from '../../fire';
import Header from "../header";

const useStyles = makeStyles({
  card: {
    display: 'flex',
  },
  cardDetails: {
    flex: 1,
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cardMedia: {
    width: 160,
  },
});


const Commande = () => {
  const [commandes, setCommandes] = useState([]);
  const [value, setValue] = useState(false);

  const classes = useStyles();
  useEffect(() => {
    setValue(true);
  });
  var commandesArray = [];

  useEffect(() => {
    var ref = fire.database().ref('Commandes');
    ref.orderByChild("idUtilisateur").equalTo(window.localStorage.uid).on('child_added', snapshot => {
      var command = { comm: snapshot.val(), id: snapshot.key }
      console.log(window.localStorage.uid);
      const obj = new Object();
      obj.idResto = command.comm.idRestaurant;
      obj.menu = command.comm.content;
      console.log(obj);
      commandesArray.push(obj);
      setCommandes(commandesArray);

    });

  }, [value]);

  return (
    <>
      <Header></Header>
      <Container maxWidth="md">
        <Typography variant="h2" align="center" color="textPrimary" gutterBottom>
          Vos Commandes        </Typography>
        <Grid display='inline-block'>

          <Offline >
            Il semblerait que vous n'êtes pas connecté à Internet, verifiez votre connexion
            <hr />
            <br></br>
          </Offline>
          {
            commandes && commandes.map((resto) =>

              <Card className={classes.root}>
                <div className={classes.cardDetails}>
                  <CardContent>

                    <Typography component="h2" variant="h5">
                      {resto.menu}
                    </Typography>
                    <Typography variant="subtitle1" color="textSecondary">
                      {resto.menu}
                    </Typography>
                    <Typography variant="subtitle1" paragraph>

                    </Typography>

                  </CardContent>
                </div>

              </Card>
            )
          }
        </Grid>
      </Container>
    </>
  );
}
export default Commande;