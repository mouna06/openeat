import Badge from '@material-ui/core/Badge';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import React from 'react';


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 151,
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: theme.spacing(4),
    paddingBottom: theme.spacing(1),
  },
  playIcon: {
    height: 38,
    width: 38,
  },
}));

const CardsMenu = ({ handlerStateMyState, menu }) => {
  const classes = useStyles();
  const theme = useTheme();
  const [invisible, setInvisible] = React.useState(false);

  const handleClickAdd = () => {
    setInvisible(!invisible);
    if (invisible === true) {
      console.log("no");
      handlerStateMyState("");
    } else {
      console.log(menu.titre);

      handlerStateMyState(menu.titre);

    }


  }
  return (
    <Badge color="secondary" invisible={!invisible}>

    <Card className={classes.root}>

        <div className={classes.details}>
          <CardContent className={classes.content}>
            <Typography component="h5" variant="h5">
              {menu.titre}
            </Typography>
            <Typography variant="subtitle1" color="textSecondary">
              {menu.titre}
            </Typography>
            <ButtonGroup>
              <Button
                aria-label="increase"
                onClick={handleClickAdd}
              >
                <AddIcon fontSize="small" />
              </Button>
            </ButtonGroup>
          </CardContent>
        </div>

        <CardMedia
          className={classes.cover}
          
          image="https://source.unsplash.com/user/ounachaibi"
          title="Live from space album cover"
        />

    </Card>
    </Badge>

  );
}
export default CardsMenu;