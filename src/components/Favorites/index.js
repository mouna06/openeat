import React, { useState, useEffect, useContext } from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { fire, db } from '../../fire'
import Cards from '../Cards';
import Header from "../header";

const Favorites = () => {
    const [restaurants, setRestaurants] = useState([]);
    const [favorites, setFavorites] = useState([]);
    const [restoFavorites, setRestoFavorites] = useState([]);
    const favoritesArray = [];
    const restoFavoritesArray = [];
    const restos = [];
    const [value, setValue] = useState(false);
    useEffect(() => {
        setValue(true);
    });
    useEffect(() => {
      console.log(favorites);

      db.collection("restaurants")
      .get()
      .then(querySnapshot => {
        const data = querySnapshot.docs.forEach(doc => {
          const resto = new Object();
          resto.id =  doc.id;
          resto.data = doc.data();
          restos.push(resto);
        });
        setRestaurants(restos); 
      });

      fire.database().ref('Favorites').orderByChild("idUtilisateur").equalTo(window.localStorage.uid).on('child_added', snapshot => {
        let favorite = { resto: snapshot.val(), id: snapshot.key }
        const obj = new Object();
        obj.idResto = favorite.resto.idRestaurant;
            favoritesArray.push(obj);
            setFavorites(favoritesArray);
      });

    }, [value]);

    restaurants && restaurants.map((resto) => {
                favorites.map((favorite) => {if(resto.id == favorite.idResto){
                    const data = new Object();
                    data.id =  resto.id;
                    data.data = resto.data;
                    restoFavoritesArray.push(data); }
            }
                )}
                )
    return (
    <>
    <Header></Header>
        <Typography variant="h2" align="center" color="textPrimary" gutterBottom>
            Vos Restaurants favoris ♥
        </Typography>
            <br></br>

        <Container  maxWidth="md">
          <Grid container spacing={4}>
                {
                restoFavoritesArray && restoFavoritesArray.map((resto) => 
                 <Cards id={resto.id} resto={resto.data}/> 
                
                )}

            </Grid>
        </Container>
    </>
    )
    ;
}

export default Favorites;
