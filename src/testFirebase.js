import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import { fire, db } from "./fire";
function Test() {

  const [messages, setMessages] = useState([]);
  const [cities, setCities] = useState([]);
  let inputEl = null;

  useEffect(() => {

    //Database RealTime
    let messagesRef = fire.database().ref('messages').orderByKey().limitToLast(100);
    messagesRef.on('child_added', snapshot => {
      /* Update React state when message is added at Firebase Database */
      let message = { text: snapshot.val(), id: snapshot.key };

      setMessages([message].concat(messages));
    })
  }, []);


    

  const addMessage = function (e) {
    e.preventDefault(); // <- prevent form submit from reloading the page
    /* Send the message to Firebase */
    fire.database().ref('messages').push( inputEl.value );
    inputEl.value = ''; // <- clear the input
  }

  return (
    <form onSubmit={addMessage}>
      <input type="text" ref={ el =>inputEl = el }/>
      <input type="submit"/>
      <ul>
        { /* Render the list of messages */
          messages.map( message => <li key={message.id}>{message.text}</li> )
        }
      </ul>
{/*       <ul>
          {
              cities.map(city => <li key={city.country}>{city.name}</li>)
          }
      </ul> */}
    </form>
  );
}

export default Test;
