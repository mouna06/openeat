module.exports = {
    globDirectory: './',
    globPatterns: ['\*\*/\*.{html,js}'],
    swDest: './src/sw.js',
    clientsClaim: true,
    skipWaiting: true
  };