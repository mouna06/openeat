import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Rating from '@material-ui/lab/Rating';
import React, { useEffect, useState } from "react";
import { fire } from '../../fire';


const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '25ch',
  },
});

const useStyles = makeStyles({
  card: {
    display: 'flex',
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
  },
});

const Comments = () => {
  const queryString = window.location.pathname;
  let inputEl = null;
  var splitPath = queryString.split("/");
  const idResto = splitPath[2];
  const [comments, setComments] = useState([]);
  const [v, setV] = useState(false);
  const classes = useStyles();
  let commentsArray = [];
  const [value, setValue] = React.useState(1);


  useEffect(() => {
    setV(true);
  });

  useEffect(() => {

    fire.database().ref('Comments').on('child_added', snapshot => {
      let comment = { com: snapshot.val(), id: snapshot.key };
      if (idResto === comment.com.idRestaurant) {
        commentsArray.push(comment);
        setComments(commentsArray);
      } else {
        commentsArray = [];
        setComments(commentsArray);
      }
    });
  }, [v]);

  return (
    <>
      <br></br>
      <Typography variant="h2" align="center" color="textPrimary" gutterBottom>
        Commentaires
        </Typography>
      <form
        onSubmit={e => {
          e.preventDefault(); // <- prevent form submit from reloading the page
          /* Send the message to Firebase */
          fire.database().ref('Comments').push({ content: inputEl.value, note: value, idRestaurant: idResto });
          inputEl.value = ''; // <- clear the input
          setValue(1);
        }}>
        <input type="text" ref={el => inputEl = el} />
        <Rating
          name="simple-controlled"
          value={value}
          onChange={(event, newValue) => {
            setValue(newValue);
          }}
        />

        <Button variant="contained" color="primary" type="Submit">Envoyer</Button>
      </form>

      <br></br>
      <br></br>
      <br></br>
      <Grid item xs={12} md={6}>

        <Card className={classes.root}>

          <div className={classes.cardDetails}>
            <CardContent>

              {comments.map((comment) =>
                      <ListItem >

                <ListItemAvatar>
                  <Avatar alt="Remy Sharp" />
                </ListItemAvatar>
                <ListItemText primary={comment.com.content}
                  secondary={
                    <React.Fragment>
                      <Typography variant="subtitle1" color="textSecondary" key={comment.id} paragraph>
                        {comment.com.content} <Rating name="read-only" value={comment.com.note} readOnly /></Typography>
                    </React.Fragment>
                  }
                />
                </ListItem>

              )}

            </CardContent>
          </div>
          <CardMedia
            className={classes.media}
            image="https://source.unsplash.com/random"
            title="{title}"
          />
        </Card>


      </Grid>
    </>
  )
    ;
}

export default Comments;
