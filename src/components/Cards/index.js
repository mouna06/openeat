import Badge from '@material-ui/core/Badge';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import { red } from "@material-ui/core/colors";
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import Rating from '@material-ui/lab/Rating';
import React, { useEffect, useState } from "react";
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { db, fire } from '../../fire';

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    width: '31%',
    margin: '1%',
  },
  media: {
    height: 140,
  },
});

const Cards = ({ id, resto }) => {
  const classes = useStyles();
  const [clicked, setClicked] = useState();
  const [value, setValue] = useState();
  const [favorites, setFavorites] = useState([]);
  const [restaurants, setRestaurants] = useState([]);
  const [notes, setNotes] = useState([]);
  var notesArray = [];
  var total = 0;
  var fav = [];
  var favesId = [];
  var restos = [];

  useEffect(() => {
    setValue(true);
  });

  const url = "/home/" + id;
  var idRemove;

  useEffect(() => {
    db.collection("restaurants")
      .get()
      .then(querySnapshot => {
        const data = querySnapshot.docs.forEach(doc => {
          const resto = new Object();
          resto.id = doc.id;
          resto.data = doc.data;
          restos.push(resto);
        });
        setRestaurants(restos);
      });

    var favoritesRef = fire.database().ref('Favorites').orderByChild("idUtilisateur").equalTo(window.localStorage.uid).on('child_added', snapshot => {

      let favorite = { resto: snapshot.val(), id: snapshot.key }
      if (favorite) {

        fav.push(favorite);
        favesId.push(favorite.resto.idRestaurant)
        setFavorites(fav);

      }
    });

    fire.database().ref('Comments').on('child_added', snapshot => {
      let note = { resto: snapshot.val(), id: snapshot.key }
      const obj = new Object();
      obj.idResto = note.resto.idRestaurant;
      obj.note = note.resto.note;
      notesArray.push(obj)
      setNotes(notesArray);
    });
  }, [value]);

  function noteTotale(id) {
    total = 0
    if (notes.length > 0) {
      var sum = 0;
      var nbNote = 0;
      notes.map(note => {
        if (note.idResto == id) {
          console.log(note.note)
          sum = note.note + sum;
          nbNote = nbNote + 1;
        }
        total = sum / nbNote;
      })
    } else total = 0

  }
  noteTotale(id)
  function favoriteResto(idR) {
    console.log('fav');
    setClicked(true);
    console.log(favorites);
    fire.database().ref('Favorites').push({ idRestaurant: idR, idUtilisateur: window.localStorage.uid });
  }

  function unfavoriteResto(id) {
    favorites.map(favorite => {
      if (favorite.resto.idRestaurant == id) idRemove = favorite.id;

    });
    setClicked(false);
    //Send the message to Firebase 
    fire.database().ref('Favorites').child(idRemove).remove();
  }

  const methodToCall = function (e) {
    console.log('1');
    console.log(favorites)
    console.log(id)

    if (favorites.length <= 0) {
      console.log('liked if nothing');
      setClicked(true);
      favoriteResto(id);
    } else {
      favorites.map(favoriteEl => {
        if (favoriteEl.resto.idRestaurant == id && favoriteEl.resto.idUtilisateur == window.localStorage.uid) {
          unfavoriteResto(id);
          setClicked(false)
          console.log('unliked')
          const index = favorites.indexOf(favoriteEl)
          favorites.splice(index)
        }
        else if ((favoriteEl.resto.idRestaurant !== id && favoriteEl.resto.idUtilisateur !== window.localStorage.uid)) {
          console.log('liked if smth ');
          setClicked(true);
          favoriteResto(id);
        }
      }
      )
    }
  }

  return (
    <Card className={classes.root}>
      <CardActionArea href={url}>

        <LazyLoadImage 
          effect="blur"
          src={"https://source.unsplash.com/user/ounachaibi" } height='300px'/>

        <CardHeader
          title={resto.nom}
          subheader={resto.adresse}
        />
      </CardActionArea>


      <CardActions disableSpacing>
        <IconButton onClick={methodToCall}>
          {clicked ? <FavoriteIcon style={{ color: red[500] }} /> : <FavoriteBorderIcon />}
        </IconButton>
        <IconButton aria-label="show 17 new notifications">
          <Rating name="half-rating-read" value={total} precision={0.5} readOnly />
          <Badge  color="secondary">
            <ChatBubbleOutlineIcon />
          </Badge>
        </IconButton>
      </CardActions>
    </Card>
  );
}

export default Cards;
