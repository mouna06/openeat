import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

const options = [];

const Search = (restos) => {
  const optionsResto = [];
  restos.restos.forEach(element => {
    const obj = { id: element.id, nom :element.data.nom}
    optionsResto.push(obj)

  });
  const optionsNom = optionsResto.map((obj) => (({ nom }) => ({nom}))(obj))
  console.log(optionsNom);
  optionsNom.map(val => {
      options.push(val.nom)
  })
  const [value, setValue] = React.useState(options[0]);
  const [inputValue, setInputValue] = React.useState("");


  return (

    <Autocomplete
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);

      }}
      inputValue={inputValue}
      onInputChange={(event, newInputValue) => {
        setInputValue(newInputValue);
        const link = optionsResto.find( val => val.nom === newInputValue )
        if(link){
          window.location.href = '/home/'+link.id;

        }
    }}
      id="controllable-states-demo"
      options={options}
      style={{ width: 300 , backgroundColor : '#ffffff99'}}
      renderInput={(params) => <TextField {...params} label="Rechercher ..." variant="outlined" />}
    />
  );
}

export default Search;